#### 说明

```
1.这是SQL Server的一个内置方法，运行此脚本后，将在SQL Server的“可编程性-函数-标量值函数”中增加一个方法 Id_GenerateChar

2.生成的ID = 时间差 + 程序号 + 随机数
	时间差 = 当前时间戳（毫秒单位）
	程序号 = @@SPID ^ @@CPU_BUSY ^ @@IDLE
	随机数 = 0 至 2^{配置值}-1

3.调用方法：
	例如：select dbo.Id_GenerateChar(RAND())
	说明：必须带dbo.前缀
	
4.自动赋值：
	如果Id为null时自动赋值，可以将该主键的 “默认值或绑定” 设置为 ([dbo].[Id_GenerateChar](rand()))，以便在Insert记录时，能给主键自动赋值，而无需外部传入。
```


#### 函数

|生成数据类型|12byte的Id|11byte的Id|10byte的Id|9byte的Id|8byte的Id|
|--|--|--|--|--|--|
|Char|Id_GenerateChar|Id11_GenerateChar|Id10_GenerateChar|Id9_GenerateChar|Id8_GenerateChar|
|Binary|Id_GenerateBinary|Id11_GenerateBinary|Id10_GenerateBinary|Id9_GenerateBinary|Id8_GenerateBinary|
|Int|-|-|-|-|Id8_GenerateInt|

#### 安装

- 安装非精准顺序Id：

> 非精准顺序Id，指Id生成出来会有小概率产生非顺序Id

1. 在数据库中，执行“TheId.ms.sql”文件的sql内容

- 安装精准顺序Id:

> 精准顺序Id，指Id生成出来一定是按照顺序产生

1. 下载App.Id.Clr.dll文件

2. 把App.Id.Clr.dll文件放在“C:\bin\App.Id.Clr.dll”目录中

3. 在数据库中，执行“TheId.ms.clr.sql”文件的sql内容

#### 示例


- 不同sql指令生成不同Id，同一sql指令生成不同Id：

```
SELECT dbo.Id_GenerateChar(CHECKSUM(NEWID())) --生成12byte的Id，再转成24byte的Char类型
SELECT dbo.Id11_GenerateChar(CHECKSUM(NEWID())) --生成11byte的Id，再转成22byteChar类型
SELECT dbo.Id10_GenerateChar(CHECKSUM(NEWID())) --生成10byte的Id，再转成20byte的Char类型
SELECT dbo.Id9_GenerateChar(CHECKSUM(NEWID())) --生成9byte的Id，再转成18byte的Char类型
SELECT dbo.Id8_GenerateChar(CHECKSUM(NEWID())) --生成8byte的Id，再转成16byte的Char类型

SELECT dbo.Id_GenerateBinary(CHECKSUM(NEWID())) --生成12byte的Id，再转成12byte的Binary类型
SELECT dbo.Id11_GenerateBinary(CHECKSUM(NEWID())) --生成11byte的Id，再转成11byte的Binary类型
SELECT dbo.Id10_GenerateBinary(CHECKSUM(NEWID())) --生成10byte的Id，再转成10byte的Binary类型
SELECT dbo.Id9_GenerateBinary(CHECKSUM(NEWID())) --生成9byte的Id，再转成9byte的Binary类型
SELECT dbo.Id8_GenerateBinary(CHECKSUM(NEWID())) --生成8byte的Id，再转成8byte的Binary类型

SELECT dbo.Id8_GenerateInt(CHECKSUM(NEWID())) --生成8byte的Id，再转成8byte的Int类型
```




- 不同sql指令生成不同Id，同一sql指令生成相同Id：

```
SELECT dbo.Id_GenerateChar(RAND()) --生成12byte的Id，再转成24byte的Char类型
SELECT dbo.Id11_GenerateChar(RAND()) --生成11byte的Id，再转成22byteChar类型
SELECT dbo.Id10_GenerateChar(RAND()) --生成10byte的Id，再转成20byte的Char类型
SELECT dbo.Id9_GenerateChar(RAND()) --生成9byte的Id，再转成18byte的Char类型
SELECT dbo.Id8_GenerateChar(RAND()) --生成8byte的Id，再转成16byte的Char类型

SELECT dbo.Id_GenerateBinary(RAND()) --生成12byte的Id，再转成12byte的Binary类型
SELECT dbo.Id11_GenerateBinary(RAND()) --生成11byte的Id，再转成11byte的Binary类型
SELECT dbo.Id10_GenerateBinary(RAND()) --生成10byte的Id，再转成10byte的Binary类型
SELECT dbo.Id9_GenerateBinary(RAND()) --生成9byte的Id，再转成9byte的Binary类型
SELECT dbo.Id8_GenerateBinary(RAND()) --生成8byte的Id，再转成8byte的Binary类型

SELECT dbo.Id8_GenerateInt(RAND()) --生成8byte的Id，再转成8byte的Int类型
```

### 自动生成Id

> 在插入数据时，自动生成分布式唯一Id。设置如下：

```
ALTER TABLE Test
	ADD Id CHAR(24) NOT NULL DEFAULT(dbo.Id_GenerateChar(CHECKSUM(NewId()))) UNIQUE
```
