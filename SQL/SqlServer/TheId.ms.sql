--(c)http://gitee.com/silis/TheId

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id_GenerateChar')
BEGIN
    DROP FUNCTION [dbo].[Id_GenerateChar]
END
GO

-- 创建函数
-- 生成12byte的Id，再转成24byte的Char数据类型
CREATE FUNCTION [dbo].[Id_GenerateChar]
(
	@rank FLOAT
)
RETURNS CHAR(24)
AS
BEGIN
	RETURN CONVERT(CHAR(24),
       CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
       CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
       CONVERT(BINARY(4), @rank) + 
	   CONVERT(BINARY(2), @@SPID ^ @@CPU_BUSY ^ @@IDLE), 2)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id11_GenerateChar')
BEGIN
    DROP FUNCTION [dbo].[Id11_GenerateChar]
END
GO

-- 创建函数
-- 生成11byte的Id，再转成22byte的Char数据类型
CREATE FUNCTION [dbo].[Id11_GenerateChar]
(
	@rank FLOAT
)
RETURNS CHAR(22)
AS
BEGIN
	RETURN CONVERT(CHAR(22),
       CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
       CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
       CONVERT(BINARY(4), @rank) + 
	   CONVERT(BINARY(1), @@SPID ^ @@CPU_BUSY ^ @@IDLE), 2)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id10_GenerateChar')
BEGIN
    DROP FUNCTION [dbo].[Id10_GenerateChar]
END
GO

-- 创建函数
-- 生成10byte的Id，再转成20byte的Char数据类型
CREATE FUNCTION [dbo].[Id10_GenerateChar]
(
	@rank FLOAT
)
RETURNS CHAR(20)
AS
BEGIN
	RETURN CONVERT(CHAR(20),
       CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
       CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
       CONVERT(BINARY(3), @rank) + 
	   CONVERT(BINARY(1), @@SPID ^ @@CPU_BUSY ^ @@IDLE), 2)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id9_GenerateChar')
BEGIN
    DROP FUNCTION [dbo].[Id9_GenerateChar]
END
GO

-- 创建函数
-- 生成9byte的Id，再转成18byte的Char数据类型
CREATE FUNCTION [dbo].[Id9_GenerateChar]
(
	@rank FLOAT
)
RETURNS CHAR(18)
AS
BEGIN
	RETURN CONVERT(CHAR(18),
       CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
       CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
       CONVERT(BINARY(2), @rank) + 
	   CONVERT(BINARY(1), @@SPID ^ @@CPU_BUSY ^ @@IDLE), 2)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id8_GenerateChar')
BEGIN
    DROP FUNCTION [dbo].[Id8_GenerateChar]
END
GO

-- 创建函数
-- 生成8byte的Id，再转成16byte的Char数据类型
CREATE FUNCTION [dbo].[Id8_GenerateChar]
(
	@rank FLOAT
)
RETURNS CHAR(16)
AS
BEGIN
	RETURN CONVERT(CHAR(16),
       CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
       CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
       CONVERT(BINARY(2), @rank), 2)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id_GenerateBinary')
BEGIN
    DROP FUNCTION [dbo].[Id_GenerateBinary]
END
GO

-- 创建函数
-- 生成12byte的Id，再转成12byte的Binary数据类型
CREATE FUNCTION [dbo].[Id_GenerateBinary]
(
	@rank FLOAT
)
RETURNS BINARY(12)
AS
BEGIN
	RETURN CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
		CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
		CONVERT(BINARY(4), @rank) + 
		CONVERT(BINARY(2), @@SPID ^ @@CPU_BUSY ^ @@IDLE)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id11_GenerateBinary')
BEGIN
    DROP FUNCTION [dbo].[Id11_GenerateBinary]
END
GO

-- 创建函数
-- 生成11byte的Id，再转成11byte的Binary数据类型
CREATE FUNCTION [dbo].[Id11_GenerateBinary]
(
	@rank FLOAT
)
RETURNS BINARY(11)
AS
BEGIN
	RETURN CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
		CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
		CONVERT(BINARY(4), @rank) + 
		CONVERT(BINARY(1), @@SPID ^ @@CPU_BUSY ^ @@IDLE)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id10_GenerateBinary')
BEGIN
    DROP FUNCTION [dbo].[Id10_GenerateBinary]
END
GO

-- 创建函数
-- 生成10byte的Id，再转成10byte的Binary数据类型
CREATE FUNCTION [dbo].[Id10_GenerateBinary]
(
	@rank FLOAT
)
RETURNS BINARY(10)
AS
BEGIN
	RETURN CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
		CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
		CONVERT(BINARY(3), @rank) + 
		CONVERT(BINARY(1), @@SPID ^ @@CPU_BUSY ^ @@IDLE)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id9_GenerateBinary')
BEGIN
    DROP FUNCTION [dbo].[Id9_GenerateBinary]
END
GO

-- 创建函数
-- 生成9byte的Id，再转成9byte的Binary数据类型
CREATE FUNCTION [dbo].[Id9_GenerateBinary]
(
	@rank FLOAT
)
RETURNS BINARY(9)
AS
BEGIN
	RETURN CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
		CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
		CONVERT(BINARY(2), @rank) + 
		CONVERT(BINARY(1), @@SPID ^ @@CPU_BUSY ^ @@IDLE)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id8_GenerateBinary')
BEGIN
    DROP FUNCTION [dbo].[Id8_GenerateBinary]
END
GO

-- 创建函数
-- 生成8byte的Id，再转成8byte的Binary数据类型
CREATE FUNCTION [dbo].[Id8_GenerateBinary]
(
	@rank FLOAT
)
RETURNS BINARY(8)
AS
BEGIN
	RETURN CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
		CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
		CONVERT(BINARY(2), @rank)
END

GO

-- 删除函数
if EXISTS(SELECT * FROM sys.objects WHERE name='Id8_GenerateInt')
BEGIN
    DROP FUNCTION [dbo].[Id8_GenerateInt]
END
GO

-- 创建函数
-- 生成8byte的Id，再转成8byte的Int数据类型
CREATE FUNCTION [dbo].[Id8_GenerateInt]
(
	@rank FLOAT
)
RETURNS BIGINT
AS
BEGIN
	RETURN CONVERT(BIGINT,
        CONVERT(BINARY(4), DATEDIFF(SECOND,'1970-01-01 00:00:00', GETUTCDATE())) +
       CONVERT(BINARY(2), DATEPART(MS, GETUTCDATE())) +
       CONVERT(BINARY(2), @rank))
END