<div align="center">
    <p align="center">
        <img src="https://images.gitee.com/uploads/images/2021/0402/133320_a1626c6f_1167316.png" alt="logo"/>
    </p>
</div>

<div align="center">

[![star](https://gitee.com/silis/TheId/badge/star.svg?theme=dark)](https://gitee.com/silis/TheId/star)
[![fork](https://gitee.com/silis/TheId/badge/fork.svg?theme=dark)](https://gitee.com/silis/TheId/fork)
[![license](https://img.shields.io/badge/license-MIT-green)](https://gitee.com/silis/TheId/license/mit)
[![free](https://img.shields.io/badge/授权-商用永久免费-green)](https://gitee.com/silis/TheId/free)

</div>

### 介绍
分布式唯一Id生成器，基于雪花算法(SnowFlake)，可以生成更快，位数更短。多语言版本让Id可以在前端、后端、数据库中多端生成，支持JS/C#/JAVA/SQL/C语言。

### 分布式Id是什么？

分布式Id，指在不同地方生成的Id基本能保证唯一，这样就不需要集中在一个服务器上管理Id的生成。

### 分布式Id的结构

一个分布式Id，占用12个字节，24个16进制字符，相当于数据库中长度char(24)

> 1个Id = 12个字节 = 24个字符 = char(24)<br>
> 1个字节 = 2个字符 = 8个bit

### JS版本示例

```
<html>
    <head>
        <script src="id.min.js"></script>
    </head>
    <body>
        <script>
            var id = Id.generateString();
            document.write(id);
        </script>
    </body>
</html>
```

### C#版本示例

> Nuget搜索SiliS.Id安装

![Nuget SiliS.Id](https://images.gitee.com/uploads/images/2021/0402/131535_4899b4c1_1167316.png "屏幕截图.png")


生成字符串类型的id

```
using App;
using System;

namespace App.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var id = Id.GenerateString();
            Console.WriteLine(id);
        }
    }
}

```

---

new一个带实体泛型的id对象

```
using App;
using System;

namespace App.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var id = new Id<MyEntity>;
            Console.WriteLine(id.ToString());
        }
    }
}

```

### Java版本示例

```
package app.test;
import app.Id;

public class Program
{
    public static void main(String []args)
    {
        String id = Id.generateString();
        System.out.println(id);
    }
}
```


### Sql Server版本示例

```
SELECT dbo.Id_GenerateChar(RAND()) --生成12byte的Id，再转成24byte的Char类型
SELECT dbo.Id11_GenerateChar(RAND()) --生成11byte的Id，再转成22byteChar类型
SELECT dbo.Id10_GenerateChar(RAND()) --生成10byte的Id，再转成20byte的Char类型
SELECT dbo.Id9_GenerateChar(RAND()) --生成9byte的Id，再转成18byte的Char类型
SELECT dbo.Id8_GenerateChar(RAND()) --生成8byte的Id，再转成16byte的Char类型

SELECT dbo.Id_GenerateBinary(RAND()) --生成12byte的Id，再转成12byte的Binary类型
SELECT dbo.Id11_GenerateBinary(RAND()) --生成11byte的Id，再转成11byte的Binary类型
SELECT dbo.Id10_GenerateBinary(RAND()) --生成10byte的Id，再转成10byte的Binary类型
SELECT dbo.Id9_GenerateBinary(RAND()) --生成9byte的Id，再转成9byte的Binary类型
SELECT dbo.Id8_GenerateBinary(RAND()) --生成8byte的Id，再转成8byte的Binary类型

SELECT dbo.Id8_GenerateInt(RAND()) --生成8byte的Id，再转成8byte的Int类型
```
> 由于自定义函数内不允许生成随机数（RAND），需要在调用方通过参数传入

### C语言示例

```
#include "id.c"

int main()
{
   char* id = Id_generateString();
   printf("%s", id);
   free(id); //需要释放内存
   
   return 0;
}
```

> 注意C语言版本，需要在调用完后主动释放id的内存
